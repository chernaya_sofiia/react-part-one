import React from 'react';

import './index.scss';

const Preloader = (): JSX.Element => (
  <div className='preloader'>Loading...</div>
);

export default Preloader;
