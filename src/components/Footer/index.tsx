import React from 'react';

import './index.scss';

function Footer() {
    return <footer>&copy; Copyright 2021 | Sophia Chernaya #BSA21</footer>;
}

export default Footer;
