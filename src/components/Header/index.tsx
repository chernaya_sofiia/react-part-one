import React from 'react';

import logoImg from '../../assets/images/logo.svg';

import './index.scss';

function Header() {
  return (
    <div className='header'>
      <img className='logo' src={logoImg} alt='Logo' />
      <h1 className='title'>Chatroom</h1>
    </div>);
}

export default Header;
