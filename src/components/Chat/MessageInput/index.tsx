import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import './index.scss';

type MessageInputProps = {
  sendMessage: (message: string) => void
  message: string
}

const MessageInput = ({ sendMessage, message }: MessageInputProps): JSX.Element => {
  const [toSend, setToSend] = useState<string>(message);
  const textArea = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    setToSend(message);
    if (textArea.current !== null) {
      textArea.current.focus();
    }
  }, [message]);

  const handleSend = (text: string) => {
    sendMessage(text);
    setToSend('');
  };

  return (
    <div className='message-input'>
      <textarea
        ref={textArea}
        className='message-input-text'
        value={toSend}
        placeholder="Write a message"
        onChange={(ev: React.ChangeEvent<HTMLTextAreaElement>) => {
          setToSend(ev.target.value);
        }}
      />
      <button
        type="button"
        className='message-input-button'
        onClick={() => handleSend(toSend)}
      >
        Send
      </button>
    </div>
  );
};

MessageInput.propTypes = {
  sendMessage: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired
};

export default MessageInput;
