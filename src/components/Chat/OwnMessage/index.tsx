import React from 'react';
import moment from 'moment';

import './index.scss';

import { IMessage } from '../../../interfaces/Message';
import { IMessageCallbacks } from '../../../interfaces/MessageCallbacks';

type MessageProps = IMessage & IMessageCallbacks;

const OwnMessage: React.FC<MessageProps> = ({
    id,
    text,
    createdAt,
    editedAt,
    user,
    likesCount,
    likeMessage,
    deleteMessage,
    editMessage
}): JSX.Element => {
    const creationTime = moment(createdAt).format('HH:mm');

    return (
        <div className='own-message'>
            <div className='message-body'>
                <div className='message-header'>
                    <span className='message-user-name'>{user}</span>
                    <div>
                        <button type="button" className='message-edit' onClick={() => editMessage(id)}>&#9998;</button>
                        <button type="button" className='message-delete' onClick={() => deleteMessage(id)}>X</button>
                    </div>
                </div>
                <div className='message-text'>
                    {text}
                </div>
                <div className='message-like-time'>
                    <div className='like-wrap'>
                        <button
                            type='button'
                            className='message-like'
                            onClick={() => likeMessage(id)}
                        >
                            &#128077;
                        </button>
                        <div className='like-count'>{likesCount}</div>
                    </div>
                    <div className='message-time'>
                        {editedAt.length ? 'edited • ' : null}
                        {creationTime}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OwnMessage;