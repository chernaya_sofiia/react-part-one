import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App/index';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root') as HTMLElement
);
