import React from 'react';

import Chat from '../Chat';
import Header from '../../components/Header';
import Footer from '../../components/Footer';

function App() {
    return (
        <div className='App'>
            <Header />
            <Chat />
            <Footer />
        </div>
    );
}

export default App;
