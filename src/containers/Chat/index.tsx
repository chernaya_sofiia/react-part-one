import React, { useState, useEffect } from 'react';
import ChatHeader from '../../components/Chat/ChatHeader';
import MessageList from '../../components/Chat/MessageList';
import MessageInput from '../../components/Chat/MessageInput';
import Preloader from '../../components/Preloader';

import { IMessage } from '../../interfaces/Message';
import { IFetchedMessages } from '../../interfaces/FetchedMessages';

import './index.scss';

const Chat = (): JSX.Element => {
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [inEditMessage, setInEditMessage] = useState<IMessage | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);

  const me = { id: '9e243930-83c9-11e9-8e0c-8f1a686f4ce', name: 'Sophie' };

  const updateMock = (fetchedMessages: IFetchedMessages[]): IMessage[] => (
    fetchedMessages.map(message => ({
      ...message,
      likesCount: 0,
      canEdit: false
    })));

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json');
        const fetchedMessages = await response.json();
        const updatedMock = updateMock(fetchedMessages);
        setMessages(updatedMock);
        setLoading(false);
      } catch (err) {
        console.warn(err);
      }
    };
    fetchData();
  }, [isLoading]);

  const sendEdited = (toSend: string) => {
    const { id } = inEditMessage as IMessage;
    const updatedMessages = messages.map(message => {
      if (message.id === id) {
        return { ...message, text: toSend, editedAt: new Date().toJSON() };
      }
      return message;
    });
    setMessages(updatedMessages);
    setInEditMessage(null);
  };

  const sendMessage = (message: string) => {
    if (inEditMessage) {
      sendEdited(message);
      return;
    }
    if (message.trim().length) {
      const newMessage = {
        id: Math.random().toString(),
        text: message,
        user: me.name,
        userId: me.id,
        createdAt: new Date(Date.now()).toISOString(),
        editedAt: '',
        canEdit: true,
        likesCount: 0
      };
      setMessages([...messages, newMessage]);
    }
  };

  const likeMessage = (id: string) => {
    const updatedMessages = messages.map(message => {
      if (message.id === id) {
        const likesCount = message.likesCount === 1 ? 0 : 1;
        return { ...message, likesCount };
      }
      return message;
    });
    setMessages(updatedMessages);
  };

  const deleteMessage = (id: string) => {
    const updatedMessages = messages.filter(message => message.id !== id);
    setMessages(updatedMessages);
  };

  const startEditMessage = (id: string) => {
    const message = messages.find(message => message.id === id);
    if (message) {
      setInEditMessage(message);
    }
  };

  return (
    isLoading
      ? <Preloader />
      : (
        <div className="chat-container">
          <ChatHeader
            name="Ozark chat"
            participants={23}
            messages={messages.length}
            lastMessage={messages[messages.length - 1].createdAt}
          />
          <MessageList
            messages={messages}
            likeMessage={likeMessage}
            deleteMessage={deleteMessage}
            editMessage={startEditMessage}
          />
          <MessageInput
            sendMessage={sendMessage}
            message={inEditMessage ? inEditMessage.text : ''}
          />
        </div>
      )
  );
};

export default Chat;
