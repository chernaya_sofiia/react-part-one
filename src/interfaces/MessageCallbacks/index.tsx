export interface IMessageCallbacks {
    likeMessage: (id: string) => void
    deleteMessage: (id: string) => void
    editMessage: (id: string) => void
}