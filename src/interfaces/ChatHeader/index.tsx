export interface IChatHeader {
    name: string,
    participants: number,
    messages: number,
    lastMessage: string
}
